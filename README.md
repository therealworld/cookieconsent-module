OXID6 CookieConsent-Module
======

![vendor-logo the-real-world.de](picture.png)

## Features

* Backend Administration of Cookies
* Cookie-Types: NECESSARY, PERFORMANCE, FUNCTIONAL, ADVERTISING
* Frontend: Selector for allowed Cookies
* Template: Script-Switch for allowed Cookies

## Module installation via composer

In order to install the module via composer run one of the following commands in commandline in your shop base directory
(where the shop's composer.json file resides).
* **composer require therealworld/cookieconsent-module** to install the actual version compatible with OXID6

## How it works (example with googleanalytics in the dynscript.tpl)

* in the backend (Admin > Service > Cookie-Admin) add a "marketing"-cookie with the ident "googleanalytics"
* change the dynscript.tpl
** place before the google-anaytics-codes:
    ```php
    [{assign var="bGoogleAnalytics" value=true}]
    [{if $oViewConf|method_exists:"isAllowedCookie" && !$oViewConf->isAllowedCookie("googleanalytics")}]
        [{assign var="bGoogleAnalytics" value=false}]
    [{/if}]
    ```
* Now you can "ask" in your code if GoogleAnalytics is allowed by visitor. Encapsulate the code:
    ```php
    [{if $bGoogleAnalytics}]
    ...
    [{/if}]
    ```

## Bugs and Issues

If you experience any bugs or issues, please report them in on https://bitbucket.org/therealworld/cookieconsent-module/issues.
