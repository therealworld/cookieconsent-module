<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Core;

use JsonException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class CookieConsentHelper
{
    /** List of CookieTypes */
    protected static ?array $_aCookieTypes = null;

    /** List of allowed Cookies */
    protected static ?array $_aAllowedCookies = null;

    /**
     * get Cookie Types.
     *
     * @throws DatabaseErrorException
     */
    public static function getCookieTypes(): array
    {
        if (is_null(self::$_aCookieTypes)) {
            self::$_aCookieTypes = ToolsDB::showEnumValues('trwcookieconsent', 'oxtype');
        }

        return self::$_aCookieTypes;
    }

    /**
     * get allowed Cookies.
     */
    public static function getAllowedCookies(): array
    {
        if (is_null(self::$_aAllowedCookies)) {
            self::$_aAllowedCookies = [];
            if ($sAllowedCookies = Registry::getUtilsServer()->getOxCookie('trwcookieconsentallowedcookies')) {
                // remove the quotes set by getOxCookie
                $sAllowedCookies = str_replace(['amp;', '&quot;'], ['', '"'], $sAllowedCookies);

                try {
                    $aAllowedCookies = json_decode($sAllowedCookies, true, 512, JSON_THROW_ON_ERROR);
                } catch (JsonException $e) {
                    $aAllowedCookies = [];
                }
                self::$_aAllowedCookies = $aAllowedCookies;
            }
        }

        return self::$_aAllowedCookies;
    }

    /** get expiration time for own cookies. */
    public static function getExpireTime(): int
    {
        $iExpire = Registry::getUtilsDate()->getTime();
        $iExpire += (int) Registry::getConfig()->getConfigParam('numTRWCookieConsentExpires');

        return $iExpire;
    }

    /** set CookieConsent Coookie */
    public static function setCookieConsentCoookie($sValue = ''): bool
    {
        return Registry::getUtilsServer()->setOxCookie(
            'trwcookieconsentallowedcookies',
            $sValue,
            self::getExpireTime(),
            '/',
            null,
            true,
            false,
            false
        );
    }
}
