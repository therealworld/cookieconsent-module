<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Core;

/**
 * Language related utility class.
 *
 * @mixin \OxidEsales\Eshop\Core\Language
 */
class Language extends Language_parent
{
    /**
     * Returns all multi language tables.
     *
     * @return array
     */
    public function getMultiLangTables()
    {
        $aTables = parent::getMultiLangTables();
        $aMultiLangTables = ['trwcookieconsent'];

        return array_merge($aTables, $aMultiLangTables);
    }
}
