<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Core;

use OxidEsales\Eshop\Application\Model\Content;
use OxidEsales\Eshop\Core\DbMetaDataHandler;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class CookieConsentEvents
{
    /**
     * OXID-Core.
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     */
    public static function onActivate()
    {
        $sCreateSql = "create table if not exists `trwcookieconsent` (
            `OXID` char(32) character set latin1 collate latin1_general_ci not null,
            `OXSHOPID` int(11) NOT NULL DEFAULT '1' COMMENT 'Shop id (oxshops)',
            `OXACTIVE` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Active',
            `OXIDENT` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Cookie Ident',
            `OXTITLE` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Cookie Title (multilanguage)',
            `OXTITLE_1` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXTITLE_2` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXTITLE_3` varchar(255) collate utf8_general_ci NOT NULL DEFAULT '' COMMENT '',
            `OXTYPE` enum('necessary','performance','functional', 'advertising') collate utf8_general_ci NOT NULL DEFAULT 'necessary' COMMENT 'Type of Cookie',
            `OXSORT` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Sorting Cookies',
            `OXTIMESTAMP` timestamp not null default current_timestamp on update current_timestamp,
            primary key (`OXID`)
            ) engine=InnoDB default CHARSET=utf8 comment 'the-real-world: Cookie Consent'";
        ToolsDB::execute($sCreateSql);

        if (ToolsConfig::isMultiShop()) {
            $sCreateSql = "create table if not exists `trwcookieconsent2shop` (
                  `OXSHOPID` int(11) NOT NULL COMMENT 'Mapped shop id',
                  `OXMAPOBJECTID` bigint(20) NOT NULL COMMENT 'Mapped object id',
                  `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
                  UNIQUE KEY `OXMAPIDX` (`OXSHOPID`,`OXMAPOBJECTID`),
                  KEY `OXMAPOBJECTID` (`OXMAPOBJECTID`),
                  KEY `OXSHOPID` (`OXSHOPID`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Mapping table for element subshop assignments';
                ";
            ToolsDB::execute($sCreateSql);

            if (!ToolsDB::tableColumnExists('trwcookieconsent', 'OXMAPID')) {
                $sAddSql = "ALTER TABLE `trwcookieconsent` ADD `OXMAPID` bigint(20) NOT NULL COMMENT 'Integer mapping identifier'";
                ToolsDB::execute($sAddSql);
            }

            if (!ToolsDB::tableIndexExists('trwcookieconsent', 'OXMAPID')) {
                $sAddSql = "ALTER TABLE `trwcookieconsent` ADD INDEX `OXMAPID` (`OXMAPID`),
                    CHANGE `OXMAPID` `OXMAPID` bigint(20) NOT NULL COMMENT 'Integer mapping identifier' AUTO_INCREMENT";
                ToolsDB::execute($sAddSql);
            }
        }

        ToolsDB::setTableMulti('trwcookieconsent');

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

        // activate OXID´s Show Cookies Notification
        foreach (ToolsConfig::getShopIds() as $iShopId) {
            ToolsConfig::saveConfigParam('blShowCookiesNotification', true, 'bool', '', $iShopId);
        }

        // create default content
        $oContent = oxNew(Content::class);
        if (!$oContent->loadByIdent('trwcookieconsentpopup')) {
            $aParams = [
                'oxloadid'  => 'trwcookieconsentpopup',
                'oxactive'  => 1,
                'oxfolder'  => 'CMSFOLDER_USERINFO',
                'oxtitle'   => 'placeholder',
                'oxcontent' => 'Lorem Ipsum',
            ];
            $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcontents');
            $oContent->assign($aParams);
            $oContent->save();
        }

        return true;
    }

    /**
     * OXID-Core.
     *
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     */
    public static function onDeactivate()
    {
        // for security set return true
        return true;
        if ($aViews = ToolsDB::getTableWithPrefix('oxv_trwcookieconsent')) {
            foreach ($aViews as $sView) {
                $sDeleteSql = "drop view `{$sView}`";
                ToolsDB::execute($sDeleteSql);
            }
        }

        ToolsDB::deleteTableMulti('trwcookieconsent');

        $sDeleteSql = 'drop table if exists `trwcookieconsent`';
        ToolsDB::execute($sDeleteSql);

        $sDeleteSql = 'drop table if exists `trwcookieconsent2shop`';
        ToolsDB::execute($sDeleteSql);

        $oMetaData = oxNew(DbMetaDataHandler::class);
        $oMetaData->updateViews();

        $oContent = oxNew(Content::class);
        if ($oContent->loadByIdent('trwcookieconsentpopup')) {
            $oContent->delete();
        }

        return true;
    }
}
