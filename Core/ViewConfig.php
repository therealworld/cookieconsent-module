<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Core;

use JsonException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Traits\ViewConfigTrait;

/**
 * View config data access class. Keeps most
 * of getters needed for formatting various urls,
 * config parameters, session information etc.
 */
class ViewConfig extends ViewConfig_parent
{
    use ViewConfigTrait;

    /** is active */
    protected ?bool $_bCookieConsentActive = null;

    /** Template getter is CookieConsent active */
    public function isCookieConsentActive(): bool
    {
        if (is_null($this->_bCookieConsentActive)) {
            $this->_bCookieConsentActive = false;
            if (
                ToolsDB::getAnyId(
                    'trwcookieconsent',
                    [
                        'oxactive' => 1,
                        'oxshopid' => Registry::getConfig()->getBaseShopId(),
                    ]
                )
            ) {
                $this->_bCookieConsentActive = true;
            }
        }

        return $this->_bCookieConsentActive;
    }

    /**
     * Template Cookie-Switch
     * You can encapsulate your code with the following condition.
     *
     * [{if $oViewConf->isAllowedCookie('demo')}]
     *   <script src="javascript_with_cookies.js">
     * [{/if}]
     *
     * @throws JsonException
     */
    public function isAllowedCookie(string $sCookieIdent = ''): bool
    {
        $bResult = false;
        $aCookies = $this->showAllowedCookie();
        if (count($aCookies) && isset($aCookies[$sCookieIdent]) && $aCookies[$sCookieIdent]) {
            $bResult = true;
        }

        return $bResult;
    }

    /**
     * Template getter show all allowed Cookies.
     *
     * @throws JsonException
     */
    public function showAllowedCookie(): array
    {
        return CookieConsentHelper::getAllowedCookies();
    }
}
