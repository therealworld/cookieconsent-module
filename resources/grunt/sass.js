const sass = require('node-sass');

module.exports = {
    moduleproduction: {
        options: {
            implementation: sass,
            update: true,
            style: 'compressed'
        },
        files: {
            "../out/src/css/cookieconsent.css": "build/scss/cookieconsent.scss"
        }
    }
};

