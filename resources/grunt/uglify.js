module.exports = {

    options: {
        preserveComments: false
    },
    moduleproduction: {
        files: {
            "../out/src/js/cookieconsent.min.js": [
                "node_modules/js-cookie/dist/js.cookie.js",
                "build/js/cookieconsent.js"
            ]
        }
    }
};