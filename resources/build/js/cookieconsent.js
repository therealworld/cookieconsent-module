
$(function () {
    var $window = $(window),
        $oAdjustSettings = $("#adjust-cookieconsent-settings"),
        $oNote = $("#cookieconsent-note"),
        $oOnlyNecessary = $("#select-only-necessary"),
        $oAll = $("#select-all"),
        $oHideSettings = $("#hide-cookieconsent-settings"),
        $oWelcome = $("#cookieconsent-welcometext"),
        $oAccordion = $("#cookieconsent-accordion"),
        $oNotNecessary = $(".cookieconsent-not-necessary"),
        $oSaveSelection = $("#save-cookieconsent-selection"),
        $oForm = $("#cookieconsent-form"),
        $oOpenPopup = $("#cookieconsent-open-popup"),
        $CookieValue = Cookies.get('trwcookieconsentallowedcookies');

    $oOpenPopup.click(function () {
        $oNote.modal({
            backdrop: "static",
            keyboard: false
        });
        $oNote.modal("show");
    });

    if ($CookieValue != undefined && $CookieValue != 'undefined') {
        $oOpenPopup.toggleClass("show");
    }
    else {
        $oOpenPopup.trigger("click");
    }

    $oAdjustSettings.click(function () {
        var $this = $(this);
        $this.css("display", "none");
        $oOnlyNecessary.css("display", "inline-block");
        $oSaveSelection.css("display", "inline-block");
        $oAll.css("display", "none");
        $oHideSettings.css("display", "inline-block");
        $oWelcome.css("display", "none");
        $oAccordion.css("display", "block");
        return false;
    });

    $oAll.click(function () {
        $oNotNecessary.prop("checked", true);
        $oSaveSelection.trigger("click");
    });

    $oOnlyNecessary.click(function () {
        $oNotNecessary.prop("checked", false);
        $oSaveSelection.trigger("click");
    });

    $oHideSettings.click(function () {
        var $this = $(this);
        $this.css("display", "none");
        $oOnlyNecessary.css("display", "none");
        $oAdjustSettings.css("display", "inline-block");
        $oSaveSelection.css("display", "none");
        $oAll.css("display", "inline-block");
        $oAccordion .css("display", "none");
        $oWelcome.css("display", "block");
        return false;
    });

    $oSaveSelection.click(function () {
        $.ajax({
            type: "POST",
            url: $oForm.attr("action"),
            data: $oForm.serialize(),
            success: function (data) {
                $oNote.modal("hide");
                $oOpenPopup.addClass("show");
            }
        });
        return false;
    });
});
