<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Component\Widget\CookieNote as OxCookieNote;
use OxidEsales\Eshop\Core\Language as OxLanguage;
use OxidEsales\Eshop\Core\ViewConfig as OxViewConfig;
use TheRealWorld\CookieConsentModule\Application\Component\Widget\CookieNote;
use TheRealWorld\CookieConsentModule\Application\Controller\Admin\CookieConsentController;
use TheRealWorld\CookieConsentModule\Application\Controller\Admin\CookieConsentList;
use TheRealWorld\CookieConsentModule\Application\Controller\Admin\CookieConsentMain;
use TheRealWorld\CookieConsentModule\Application\Controller\CookieConsentDetailsController;
use TheRealWorld\CookieConsentModule\Core\Language;
use TheRealWorld\CookieConsentModule\Core\ViewConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwcookieconsent',
    'title' => [
        'de' => 'the-real-world - Cookie Zustimmung',
        'en' => 'the-real-world - Cookie Consent',
    ],
    'description' => [
        'de' => 'Das Modul ermöglicht die Unterscheidung von Cookies nach Notwendigkeit, Performance, Funktion und Marketing. Die Cookies und die dazu gehörenden Scripte können vom Besucher abgewählt werden.',
        'en' => 'The module enables cookies to be differentiated according to necessity, performance, function and marketing. The visitor can unselect the cookies and the associated scripts.',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwcookieconsent'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\CookieConsentModule\Core\CookieConsentEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\CookieConsentModule\Core\CookieConsentEvents::onDeactivate',
    ],
    'extend' => [
        // Core
        OxLanguage::class   => Language::class,
        OxViewConfig::class => ViewConfig::class,
        // Components
        OxCookieNote::class => CookieNote::class,
    ],
    'controllers' => [
        'CookieConsentController'        => CookieConsentController::class,
        'CookieConsentList'              => CookieConsentList::class,
        'CookieConsentMain'              => CookieConsentMain::class,
        'CookieConsentDetailsController' => CookieConsentDetailsController::class,
    ],
    'templates' => [
        'cookieconsent.tpl'                   => 'trw/trwcookieconsent/Application/views/admin/tpl/cookieconsent.tpl',
        'cookieconsentlist.tpl'               => 'trw/trwcookieconsent/Application/views/admin/tpl/cookieconsentlist.tpl',
        'cookieconsentmain.tpl'               => 'trw/trwcookieconsent/Application/views/admin/tpl/cookieconsentmain.tpl',
        'layout/cookieconsentnotecss.tpl'     => 'trw/trwcookieconsent/Application/views/theme/tpl/layout/cookieconsentnotecss.tpl',
        'widget/header/cookieconsentnote.tpl' => 'trw/trwcookieconsent/Application/views/theme/tpl/widget/header/cookieconsentnote.tpl',
    ],
    'blocks' => [
        [
            'template' => 'layout/base.tpl',
            'block'    => 'base_style',
            'file'     => 'Application/views/blocks/layout/base_style.tpl',
        ],
        [
            'template' => 'bottomnaviitem.tpl',
            'block'    => 'admin_bottomnavicustom',
            'file'     => 'Application/views/blocks/admin_bottomnavicustom.tpl',
        ],
        [
            'template' => 'shop_mall.tpl',
            'block'    => 'admin_shop_mall_inheritance',
            'file'     => 'Application/views/blocks/admin_shop_mall_inheritance.tpl',
        ],
    ],
    'settings' => [
        [
            'group' => 'trwcookieconsentoptions',
            'name'  => 'numTRWCookieConsentExpires',
            'type'  => 'num',
            'value' => 31536000,
        ],
    ],
];
