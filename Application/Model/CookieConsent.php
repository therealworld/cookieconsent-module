<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Model;

use OxidEsales\Eshop\Core\Model\MultiLanguageModel;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Cookie-Consent-Manager.
 * Performs Cookie-Consent data/objects loading, deleting.
 */
class CookieConsent extends MultiLanguageModel
{
    use DataGetter;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_blUseLazyLoading = true;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sClassName = 'TheRealWorld\CookieConsentModule\Application\Model\CookieConsent';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function __construct()
    {
        parent::__construct();
        $this->init('trwcookieconsent');
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getSqlActiveSnippet($blForceCoreTable = null)
    {
        $sTable = $this->getViewName($blForceCoreTable);

        return " {$sTable}.`oxactive` = 1 ";
    }
}
