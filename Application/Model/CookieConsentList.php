<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Model;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Model\ListModel;

/**
 * CookieConsentList class.
 */
class CookieConsentList extends ListModel
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sObjectsInListName = 'TheRealWorld\CookieConsentModule\Application\Model\CookieConsent';

    /**
     * Loads all Cookie-Consent-Entries.
     *
     * @param int   $iStep       - Start position for SQL
     * @param int   $iLimit      - Limit position for SQL
     * @param bool  $bOnlyActive - load only active Manufacturers?
     * @param array $aConditions - more Conditions for SQL
     *
     * @throws DatabaseConnectionException
     */
    public function loadAllCookieConsentEntries(
        int $iStep = 0,
        int $iLimit = 0,
        bool $bOnlyActive = true,
        array $aConditions = []
    ): void {
        $oBaseObject = $this->getBaseObject();
        $sViewName = $oBaseObject->getViewName();
        $sCookieConsentFields = $oBaseObject->getSelectFields();
        $oDb = DatabaseProvider::getDb();

        $sLimit = $iLimit ? ' limit ' . $iStep . ', ' . $iLimit : '';

        $sVars = '';
        $sOnlyActive = '';

        // more conditions
        if (is_array($aConditions) && count($aConditions)) {
            foreach ($aConditions as $aCondition) {
                // check if all necessary keys exists
                if (
                    count(
                        array_diff_key(
                            array_flip(
                                ['operator', 'condition', 'field', 'value', 'noquote']
                            ),
                            $aCondition
                        )
                    ) === 0
                ) {
                    $sVars .= ' ' . $aCondition['operator'];
                    $sVars .= " {$aCondition['field']} " . $aCondition['condition'] . ' '
                        . ($aCondition['noquote'] ? $aCondition['value'] : $oDb->quote($aCondition['value']));
                }
            }
        }

        // active?
        if ($bOnlyActive) {
            $sOnlyActive .= ' and ' . $oBaseObject->getSqlActiveSnippet();
        }

        $sSelect = "select {$sCookieConsentFields}
            from {$sViewName}
            where 1 " . $sVars . $sOnlyActive . $sLimit;

        $this->selectString($sSelect);
    }
}
