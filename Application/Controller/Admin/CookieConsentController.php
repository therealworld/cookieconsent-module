<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;

/**
 * Admin cookie consent manager.
 * Returns template, that arranges two other templates ("cookieconsentlist.tpl"
 * and "cookieconsentmain.tpl") to frame.
 * Admin Menu: Service -> Cookieconsent.
 */
class CookieConsentController extends AdminController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'cookieconsent.tpl';
}
