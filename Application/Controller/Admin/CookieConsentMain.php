<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Controller\Admin;

use Exception;
use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Application\Model\Content;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use stdClass;
use TheRealWorld\CookieConsentModule\Application\Model\CookieConsent;
use TheRealWorld\CookieConsentModule\Core\CookieConsentHelper;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

class CookieConsentMain extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'cookieconsentmain.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseErrorException
     */
    public function render()
    {
        parent::render();

        // check if we right now saved a new entry
        $sOxId = $this->_aViewData['oxid'] = $this->getEditObjectId();
        if ($sOxId !== '-1' && isset($sOxId)) {
            // load object
            $oCookieConsent = oxNew(CookieConsent::class);
            $oCookieConsent->loadInLang($this->_iEditLang, $sOxId);

            $oOtherLang = $oCookieConsent->getAvailableInLangs();
            if (!isset($oOtherLang[$this->_iEditLang])) {
                // echo "language entry doesn't exist! using: ".key($oOtherLang);
                $oCookieConsent->loadInLang(key($oOtherLang), $sOxId);
            }

            $this->_aViewData['edit'] = $oCookieConsent;

            // remove already created languages
            $aLang = array_diff(Registry::getLang()->getLanguageNames(), $oOtherLang);

            if (count($aLang)) {
                $this->_aViewData['posslang'] = $aLang;
            }

            foreach ($oOtherLang as $id => $language) {
                $oLang = new stdClass();
                $oLang->sLangDesc = $language;
                $oLang->selected = ($id === $this->_iEditLang);
                $this->_aViewData['otherlang'][$id] = clone $oLang;
            }

            // Disable editing for derived items
            if ($oCookieConsent->isDerived()) {
                $this->_aViewData['readonly'] = true;
            }
        }

        $this->_aViewData['cookieTypes'] = CookieConsentHelper::getCookieTypes();

        return $this->_sThisTemplate;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function save()
    {
        parent::save();

        $this->_checkPopUpContent();

        $sOxId = $this->getEditObjectId();

        $aParams = Registry::getRequest()->getRequestParameter('editval');

        $oCookieConsent = oxNew(CookieConsent::class);

        if ($sOxId !== '-1') {
            $oCookieConsent->load($sOxId);
            $oCookieConsent->loadInLang($this->_iEditLang, $sOxId);
        } else {
            $aParams['trwcookieconsent__oxid'] = null;
        }

        $oCookieConsent->assign($aParams);
        $oCookieConsent->setLanguage($this->_iEditLang);

        $oCookieConsent->save();

        $this->setEditObjectId($oCookieConsent->getId());
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function saveinnlang()
    {
        $this->save();
    }

    /** check the content of the cookieconsent-Popup */
    protected function _checkPopUpContent(): void
    {
        // create default content
        $oContent = oxNew(Content::class);
        if ($oContent->loadByIdent('trwcookieconsentpopup') && $oContent->getTitle() === 'placeholder') {
            $oLang = Registry::getLang();

            foreach ($oLang->getAllShopLanguageIds() as $iLang => $sLang) {
                $oContent->setLanguage($iLang);
                $aParams = [
                    'oxtitle'   => $oLang->translateString('TRWCOOKIECONSENT_POPUP_HEADLINE', $iLang),
                    'oxcontent' => $oLang->translateString('TRWCOOKIECONSENT_WELCOME', $iLang),
                ];
                $aParams = ToolsDB::convertDB2OxParams($aParams, 'oxcontents');
                $oContent->assign($aParams);
                $oContent->save();
            }
        }
    }
}
