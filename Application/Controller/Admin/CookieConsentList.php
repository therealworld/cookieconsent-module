<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use TheRealWorld\CookieConsentModule\Core\CookieConsentHelper;

/**
 * Admin cookieconsent main manager.
 * Performs collection and managing (such as filtering or deleting) function.
 * Admin Menu: Service -> Cookieconsent.
 */
class CookieConsentList extends AdminListController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sListClass = 'TheRealWorld\CookieConsentModule\Application\Model\CookieConsent';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sDefSortField = 'oxtitle';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'cookieconsentlist.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseErrorException
     */
    public function render()
    {
        parent::render();

        $this->_aViewData['cookieTypes'] = CookieConsentHelper::getCookieTypes();

        return $this->_sThisTemplate;
    }
}
