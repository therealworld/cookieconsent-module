<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Controller;

use JsonException;
use OxidEsales\Eshop\Application\Controller\FrontendController;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\CookieConsentModule\Core\CookieConsentHelper;

/**
 * CookieConsentDetailsController Class.
 */
class CookieConsentDetailsController extends FrontendController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        Registry::getUtils()->showMessageAndExit('');
    }

    /**
     * set Cookies to Session (as Cookie).
     *
     * @throws JsonException
     */
    public function setCookiesToSession(): void
    {
        $aRequestParams = Registry::getRequest()->getRequestParameter('aAllowedCookies');
        $aAllowedCookies = [];
        foreach ($aRequestParams as $sIdent => $sValue) {
            $aAllowedCookies[$sIdent] = $sValue === '1';
        }

        CookieConsentHelper::setCookieConsentCoookie(
            json_encode($aAllowedCookies, JSON_THROW_ON_ERROR)
        );
    }
}
