<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\CookieConsentModule\Application\Component\Widget;

use JsonException;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use stdClass;
use TheRealWorld\CookieConsentModule\Application\Model\CookieConsentList;
use TheRealWorld\CookieConsentModule\Core\CookieConsentHelper;

class CookieNote extends CookieNote_parent
{
    /** Overload-Template for Cookie-Note. The Original Template has no blocks... */
    protected string $_sOverloadTemplate = 'widget/header/cookieconsentnote.tpl';

    /**
     * List of Cookie Consents.
     */
    protected array $_aCookieConsentLists = [];

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        if (!Registry::getUtilsServer()->getOxCookie('trwcookieconsentallowedcookies')) {
            CookieConsentHelper::setCookieConsentCoookie(
                'undefined'
            );
        }

        parent::render();

        return $this->_sOverloadTemplate;
    }

    /** get Cookie Types.
     * @throws DatabaseErrorException
     */
    public function getCookieTypes(): array
    {
        return CookieConsentHelper::getCookieTypes();
    }

    /** get Cookie Types.
     *
     * @throws DatabaseConnectionException
     * @throws JsonException
     */
    public function getCookiesByType(string $sCookieType = ''): array
    {
        $sCookieType ??= 'all';

        if (!isset($this->_aCookieConsentLists[$sCookieType])) {
            $this->_aCookieConsentLists[$sCookieType] = [];
            $aConditions = [];
            if ($sCookieType !== 'all') {
                $aConditions = [[
                    'operator'  => 'and',
                    'field'     => 'oxtype',
                    'value'     => $sCookieType,
                    'condition' => '=',
                    'noquote'   => false,
                ]];
            }

            $aAllowedCookies = CookieConsentHelper::getAllowedCookies();
            $oCookieConsentList = oxNew(CookieConsentList::class);
            $oCookieConsentList->loadAllCookieConsentEntries(0, 0, true, $aConditions);
            if ($oCookieConsentList->count()) {
                foreach ($oCookieConsentList as $oCookieConsentItem) {
                    $sCookieIdent = $oCookieConsentItem->getTRWStringData('oxident');
                    $oCookieConsent = new stdClass();
                    $oCookieConsent->ident = $sCookieIdent;
                    $oCookieConsent->title = $oCookieConsentItem->getTRWStringData('oxtitle');
                    $oCookieConsent->allowed = isset($aAllowedCookies[$sCookieIdent])
                        && $aAllowedCookies[$sCookieIdent];
                    $this->_aCookieConsentLists[$sCookieType][] = clone $oCookieConsent;
                }
            }
        }

        return $this->_aCookieConsentLists[$sCookieType];
    }
}
