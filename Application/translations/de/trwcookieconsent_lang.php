<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                           => 'UTF-8',
    'TRWCOOKIECONSENT_TYPE_NECESSARY'   => 'Notwendig',
    'TRWCOOKIECONSENT_TYPE_PERFORMANCE' => 'Nutzerverhalten',
    'TRWCOOKIECONSENT_TYPE_FUNCTIONAL'  => 'Funktional',
    'TRWCOOKIECONSENT_TYPE_ADVERTISING' => 'Marketing',
    'TRWCOOKIECONSENT_ONLY_NECESSARY'   => 'Nur Notwendige auswählen',
    'TRWCOOKIECONSENT_SAVE_ALL'         => 'Alle akzeptieren',
    'TRWCOOKIECONSENT_SAVE'             => 'Speichern',
    'TRWCOOKIECONSENT_SETTINGS'         => 'Einstellungen',
    'TRWCOOKIECONSENT_BACK'             => 'Zurück',
];
