<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                           => 'UTF-8',
    'TRWCOOKIECONSENT_TYPE_NECESSARY'   => 'Necessary',
    'TRWCOOKIECONSENT_TYPE_PERFORMANCE' => 'Performance',
    'TRWCOOKIECONSENT_TYPE_FUNCTIONAL'  => 'Functional',
    'TRWCOOKIECONSENT_TYPE_ADVERTISING' => 'Marketing',
    'TRWCOOKIECONSENT_ONLY_NECESSARY'   => 'select only Necessary',
    'TRWCOOKIECONSENT_SAVE'             => 'Save',
    'TRWCOOKIECONSENT_SAVE_ALL'         => 'Accept all',
    'TRWCOOKIECONSENT_SETTINGS'         => 'Settings',
    'TRWCOOKIECONSENT_BACK'             => 'Back',
];
