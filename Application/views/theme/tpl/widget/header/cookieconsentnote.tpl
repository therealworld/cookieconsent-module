[{if $oViewConf|method_exists:'isCookieConsentActive' && $oViewConf->isCookieConsentActive() && $oView->isEnabled()}]
    [{oxscript include="js/libs/jquery.cookie.min.js"}]

    [{oxscript include=$oViewConf->getModuleUrl('trwcookieconsent', 'out/src/js/cookieconsent.min.js') priority=8}]
    [{assign var="popupTitle" value=""}]
    [{assign var="popupDesc" value=""}]
    [{oxifcontent ident="trwcookieconsentpopup" object="oCont"}]
        [{assign var="popupTitle" value=$oCont->oxcontents__oxtitle->value}]
        [{capture assign="popupDesc"}][{oxcontent ident="trwcookieconsentpopup"}][{/capture}]
    [{/oxifcontent}]
    <div id="cookieconsent-note" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">
                        [{if $oViewConf->isWaveCompatibleTheme()}]
                            <i class="fas fa-shield-alt"></i>
                        [{else}]
                            <i class="fa fa-shield" aria-hidden="true"></i>
                        [{/if}]
                        [{$popupTitle}]
                    </h4>
                </div>
                <form id="cookieconsent-form" action="[{$oViewConf->getSelfActionLink()}]" method="get">
                    [{$oViewConf->getNavFormParams()}]
                    [{$oViewConf->getHiddenSid()}]
                    <input type="hidden" name="cl" value="CookieConsentDetailsController">
                    <input type="hidden" name="fnc" value="setCookiesToSession">
                    [{assign var="aCookieConsentTypes" value=$oView->getCookieTypes()}]
                    <div class="modal-body">
                        <div id="cookieconsent-welcometext">[{$popupDesc}]</div>
                        [{if $aCookieConsentTypes|@count > 0}]
                            <div class="panel-group row" id="cookieconsent-accordion" role="tablist" aria-multiselectable="true" style="display:none;">
                                [{* for debugging: $oViewConf->showAllowedCookie()|@var_dump *}]
                                [{foreach from=$aCookieConsentTypes item="sCookieConsentType" name="cookieconsenttypes"}]
                                    [{assign var="oCookies" value=$oView->getCookiesByType($sCookieConsentType)}]
                                    [{assign var="iCookieTypesIteration" value=$smarty.foreach.cookieconsenttypes.iteration}]
                                    [{if $oCookies}]
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading[{$smarty.foreach.cookieconsenttypes.iteration}]">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse"
                                                        data-parent="#cookieconsent-accordion"
                                                        href="#collapse[{$iCookieTypesIteration}]"
                                                        aria-expanded="true"
                                                        aria-controls="collapse[{$iCookieTypesIteration}]">
                                                        [{oxmultilang ident="TRWCOOKIECONSENT_TYPE_"|cat:$sCookieConsentType|upper}] <i class="fa fa-caret-down pull-right"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse[{$iCookieTypesIteration}]"
                                                class="panel-collapse collapse[{if $smarty.foreach.cookieconsenttypes.first}] in show[{/if}]"
                                                role="tabpanel"
                                                aria-labelledby="heading[{$iCookieTypesIteration}]">
                                                <ul class="panel-body list list-unstyled">
                                                    [{foreach from=$oCookies item="oCookie" name="cookieconsentinner"|cat:$sCookieConsentType}]
                                                        <li>
                                                            <label class="btn-block">
                                                                <input type="hidden"
                                                                    name="aAllowedCookies[[{$oCookie->ident}]]"
                                                                    value="[{if $sCookieConsentType == 'necessary'}]1[{else}]0[{/if}]">
                                                                <input class="cookieconsent-[{if $sCookieConsentType !== 'necessary'}]not-[{/if}]necessary"
                                                                    type="checkbox"
                                                                    name="aAllowedCookies[[{$oCookie->ident}]]"
                                                                    value="1"
                                                                    [{if $sCookieConsentType == 'necessary' || $oCookie->allowed}]checked[{/if}]
                                                                    [{if $sCookieConsentType == 'necessary'}]disabled="disabled"[{/if}]>
                                                                <strong>[{$oCookie->title}]</strong>
                                                            </label>
                                                        </li>
                                                    [{/foreach}]
                                                </ul>
                                            </div>
                                        </div>
                                    [{/if}]
                                [{/foreach}]
                            </div>
                        [{/if}]
                    </div>
                    <div class="modal-footer">
                        <button id="hide-cookieconsent-settings" type="button" class="btn btn-default btn-md" style="display:none">
                            [{oxmultilang ident='TRWCOOKIECONSENT_BACK'}]
                        </button>
                        <button id="select-all" type="button" class="btn btn-primary btn-md">
                            <strong>[{oxmultilang ident='TRWCOOKIECONSENT_SAVE_ALL'}]</strong>
                        </button>
                        <button id="select-only-necessary" type="button" class="btn btn-default btn-md" style="display:none">
                            [{oxmultilang ident='TRWCOOKIECONSENT_ONLY_NECESSARY'}]
                        </button>
                        <button id="save-cookieconsent-selection" type="button" class="btn btn-default btn-md" style="display:none">
                            [{oxmultilang ident='TRWCOOKIECONSENT_SAVE'}]
                        </button>
                        <button id="adjust-cookieconsent-settings" type="button" class="btn btn-default btn-md">
                            [{oxmultilang ident='TRWCOOKIECONSENT_SETTINGS'}]
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    [{if $oViewConf->isWaveCompatibleTheme()}]
        <i class="fas fa-2x fa-shield-alt fa-inverse bg-primary" id="cookieconsent-open-popup"></i>
    [{else}]
        <i class="fa fa-2x fa-shield fa-inverse bg-primary" id="cookieconsent-open-popup" aria-hidden="true"></i>
    [{/if}]
[{/if}]
[{oxscript widget=$oView->getClassName()}]