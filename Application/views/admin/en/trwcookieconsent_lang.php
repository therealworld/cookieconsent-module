<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'en' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'mxtrwcookieconsent'     => 'Cookie-Admin',
        'tbclcookieconsent_main' => 'Cookie-Admin',
        'cookieconsent_new'      => 'New Cookie',

        'TRWCOOKIECONSENT_TEMPLATE_TITLE' => '[Cookie Consent]',

        'GENERAL_COOKIECONSENT_IDENT' => 'Cookie ID',
        'GENERAL_COOKIECONSENT_TITLE' => 'Cookie title',
        'GENERAL_COOKIECONSENT_TYPE'  => 'Cookie Type',

        'TOOLTIPS_NEWCOOKIECONSET' => 'Create New Cookie',

        'TRWCOOKIECONSENT_POPUP_HEADLINE' => 'Settings for your Privacy',
        'TRWCOOKIECONSENT_WELCOME'        => 'We use technologies such as cookies and process personal data such as IP addresses or browser information to measure results, coordinate the content of our website and improve your shopping experience. We ask for your approval.<br />(<a href="[{oxgetseourl ident="oxsecurityinfo" type="oxcontent"}]">More Informations about Privacy</a>)',

        'SHOP_MALL_MALLINHERIT_TRWCOOKIECONSENT' => 'Inherit all <b>(TRW) cookies</b> from parent shop',
    ]
);
