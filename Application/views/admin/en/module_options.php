<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcookieconsentoptions' => 'Options',

    'SHOP_MODULE_numTRWCookieConsentExpires'      => 'Own cookies expiry time',
    'HELP_SHOP_MODULE_numTRWCookieConsentExpires' => 'After how many seconds do the two system-relevant cookies "trwcookieconsent" and "trwcookieconsentallowedcookies", which are required to control the cookies, expire? (Example: 31536000 = 1 year)',
];
