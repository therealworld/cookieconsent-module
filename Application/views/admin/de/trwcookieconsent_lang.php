<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

include_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' .
    DIRECTORY_SEPARATOR . 'translations' . DIRECTORY_SEPARATOR .
    'de' .
    DIRECTORY_SEPARATOR . basename(__FILE__);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array_merge(
    $aLang,
    [
        'mxtrwcookieconsent'     => 'Cookie-Verwaltung',
        'tbclcookieconsent_main' => 'Cookie-Verwaltung',
        'cookieconsent_new'      => 'Neuer Cookie',

        'TRWCOOKIECONSENT_TEMPLATE_TITLE' => '[Cookie Zustimmung]',

        'GENERAL_COOKIECONSENT_IDENT' => 'Cookie ID',
        'GENERAL_COOKIECONSENT_TITLE' => 'Bezeichnung des Cookies',
        'GENERAL_COOKIECONSENT_TYPE'  => 'Cookie Typ',

        'TOOLTIPS_NEWCOOKIECONSET' => 'Neuen Cookie anlegen',

        'TRWCOOKIECONSENT_POPUP_HEADLINE' => 'Einstellungen für Ihre Privatsphäre',
        'TRWCOOKIECONSENT_WELCOME'        => 'Wir verwenden Technologien wie Cookies und verarbeiten personenbezogene Daten wie IP-Adressen oder Browserinformationen, um Ergebnisse zu messen, Inhalte unserer Website abzustimmen und Ihr Einkaufserlebnis zu verbessern. Wir bitten Sie um Ihre Zustimmung.<br />(<a href="[{oxgetseourl ident="oxsecurityinfo" type="oxcontent"}]">Weitere Informationen zum Datenschutz</a>)',

        'SHOP_MALL_MALLINHERIT_TRWCOOKIECONSENT' => 'Alle <b>(TRW) Cookies</b> vom Elternshop erben',
    ]
);
