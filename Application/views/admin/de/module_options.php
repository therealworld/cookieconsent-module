<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwcookieconsentoptions' => 'Optionen',

    'SHOP_MODULE_numTRWCookieConsentExpires'      => 'Verfallszeit eigene Cookies',
    'HELP_SHOP_MODULE_numTRWCookieConsentExpires' => 'Nach wieviel Sekunden verfallen die beiden systemrelevanten Cookies "trwcookieconsent" und "trwcookieconsentallowedcookies", die zur Steuerung der Cookies benötigt werden? (Bsp: 31536000 = 1 Jahr)',
];
