[{include file="headitem.tpl" title="TRWCOOKIECONSENT_TEMPLATE_TITLE"|oxmultilangassign box="list"}]
[{assign var="where" value=$oView->getListFilter()}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<script type="text/javascript">
    <!--
    window.onload = function ()
    {
        top.reloadEditFrame();
        [{if $updatelist == 1}]
            top.oxid.admin.updateList('[{$oxid}]');
        [{/if}]
    }
    //-->
</script>

<div id="liste">
    <form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
        [{include file="_formparams.tpl" cl="cookieconsentlist" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang}]
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <colgroup>
                [{block name="admin_trwcookieconsent_list_colgroup"}]
                    <col width="7%">
                    <col width="43%">
                    <col width="45%">
                    <col width="5%">
                [{/block}]
            </colgroup>

            <tr class="listitem">
                [{block name="admin_trwcookieconsent_list_filter"}]
                    <td class="listfilter first" height="20">&nbsp;</td>
                    <td class="listfilter" height="20">
                        <input class="listedit" type="text" size="50" maxlength="128" name="where[trwcookieconsent][oxtitle]" value="[{$where.trwcookieconsent.oxtitle}]">
                    </td>
                    <td class="listfilter" height="20">
                        <div class="r1">
                            <div class="b1">
                                <select class="folderselect" name="where[trwcookieconsent][oxtype]" onchange="document.search.submit(); return false;">
                                    <option value="">---</option>
                                    [{foreach from=$cookieTypes item="sCookieType" name="cookietypes"}]
                                        <option value="[{$sCookieType}]" [{if $where.trwcookieconsent.oxtype == $sCookieType}]selected[{/if}]>
                                            [{oxmultilang ident="TRWCOOKIECONSENT_TYPE_"|cat:$sCookieType|upper}]
                                        </option>
                                    [{/foreach}]
                                </select>
                            </div>
                        </div>
                    </td>
                    <td class="listfilter" height="20">
                        <input class="listedit" type="submit" name="submitit" value="[{oxmultilang ident="GENERAL_SEARCH"}]">
                    </td>
                [{/block}]
            </tr>
            <tr>
                [{block name="admin_trwcookieconsent_list_sorting"}]
                    <td class="listheader first" height="15" width="30" align="center">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwcookieconsent', 'oxactive', 'asc');document.search.submit(); return false;" class="listheader">[{ oxmultilang ident="GENERAL_ACTIVTITLE" }]</a>
                    </td>
                    <td class="listheader">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwcookieconsent', 'oxtitle', 'asc');document.search.submit(); return false;" class="listheader">[{oxmultilang ident="GENERAL_COOKIECONSENT_TITLE"}]</a>
                    </td>
                    <td class="listheader">
                        <a href="#" onclick="top.oxid.admin.setSorting(document.search, 'trwcookieconsent', 'oxident', 'asc');document.search.submit(); return false;" class="listheader">[{oxmultilang ident="GENERAL_COOKIECONSENT_TYPE"}]</a>
                    </td>
                    <td class="listheader">
                    </td>
                [{/block}]
            </tr>

            [{assign var="blWhite" value=""}]
            [{assign var="_cnt" value=0}]
            [{foreach from=$mylist item=listitem}]
                [{assign var="_cnt" value=$_cnt+1}]
                <tr id="row.[{$_cnt}]">
                    [{block name="admin_trwcookieconsent_list_item"}]
                        [{if $listitem->blacklist == 1}]
                            [{assign var="listclass" value=listitem3 }]
                        [{else}]
                            [{assign var="listclass" value=listitem$blWhite }]
                        [{/if}]
                        [{if $listitem->getId() == $oxid }]
                            [{assign var="listclass" value=listitem4 }]
                        [{/if}]
                        <td valign="top" class="[{$listclass}][{if $listitem->trwcookieconsent__oxactive->value == 1}] active[{/if}]" height="15">
                            <div class="listitemfloating">&nbsp;</div>
                        </td>
                        <td valign="top" class="[{$listclass}]" height="15">
                            <div class="listitemfloating">
                                <a href="#" onclick="top.oxid.admin.editThis('[{$listitem->trwcookieconsent__oxid->value}]');" class="[{$listclass}]">
                                    [{$listitem->trwcookieconsent__oxtitle->value}]
                                </a>
                            </div>
                        </td>
                        <td valign="top" class="[{$listclass}]" height="15">
                            <div class="listitemfloating">
                                <a href="#" onclick="top.oxid.admin.editThis('[{$listitem->trwcookieconsent__oxid->value}]');" class="[{$listclass}]">
                                    [{oxmultilang ident="TRWCOOKIECONSENT_TYPE_"|cat:$listitem->trwcookieconsent__oxtype->value|upper}]
                                </a>
                            </div>
                        </td>
                        <td class="[{$listclass}]">
                            <a href="#" onclick="top.oxid.admin.deleteThis('[{$listitem->trwcookieconsent__oxid->value}]');" class="delete" id="del.[{$_cnt}]" [{include file="help.tpl" helpid=item_delete}]></a>
                        </td>
                    [{/block}]
                </tr>
                [{if $blWhite == "2"}]
                    [{assign var="blWhite" value=""}]
                [{else}]
                    [{assign var="blWhite" value="2"}]
                [{/if}]
            [{/foreach}]
            [{include file="pagenavisnippet.tpl" colspan="4"}]
        </table>
    </form>
</div>

[{include file="pagetabsnippet.tpl"}]


<script type="text/javascript">
    if (parent.parent)
    {
        parent.parent.sShopTitle   = "[{$actshopobj->oxshops__oxname->getRawValue()|oxaddslashes}]";
        parent.parent.sMenuItem    = "[{oxmultilang ident="GENERAL_MENUITEM"}]";
        parent.parent.sMenuSubItem = "[{oxmultilang ident="TRWCOOKIECONSENT_TEMPLATE_TITLE"}]";
        parent.parent.sWorkArea    = "[{$_act}]";
        parent.parent.setTitle();
    }
</script>
</body>
</html>

