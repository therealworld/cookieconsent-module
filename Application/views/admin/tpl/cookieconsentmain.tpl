[{include file="headitem.tpl" title="TRWCOOKIECONSENT_TEMPLATE_TITLE"|oxmultilangassign}]

[{if $readonly }]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="CookieConsentMain">
</form>

<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="CookieConsentMain">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="editval[trwcookieconsent__oxid]" value="[{$oxid}]">

    <table cellspacing="0" cellpadding="0" border="0" width="98%">
        <tr>
            <td valign="top" class="edittext">
                <table cellspacing="0" cellpadding="0" border="0">
                    [{block name="admin_trwcookieconsent_main_form"}]
                    <tr>
                        <td class="edittext" width="90">
                            [{oxmultilang ident="GENERAL_ACTIVE"}]
                        </td>
                        <td class="edittext">
                            <input type="hidden" name="editval[trwcookieconsent__oxactive]" value="0" />
                            <input class="edittext" type="checkbox" name="editval[trwcookieconsent__oxactive]" value="1" [{if $edit->trwcookieconsent__oxactive->value == 1}]checked[{/if}] [{$readonly}] />
                            [{oxinputhelp ident="HELP_GENERAL_ACTIVE"}]
                        </td>
                    </tr>
                    <tr>
                        <td class="edittext">
                            [{oxmultilang ident="GENERAL_COOKIECONSENT_IDENT"}]
                        </td>
                        <td class="edittext">
                            <input type="text" class="editinput" size="25" maxlength="[{$edit->trwcookieconsent__oxident->fldmax_length}]" name="editval[trwcookieconsent__oxident]" value="[{$edit->trwcookieconsent__oxident->value}]" [{$readonly}] />
                            [{oxinputhelp ident="HELP_GENERAL_COOKIECONSENT_IDENT"}]
                        </td>
                    </tr>
                    <tr>
                        <td class="edittext">
                            [{oxmultilang ident="GENERAL_NAME"}]
                        </td>
                        <td class="edittext">
                            <input type="text" class="editinput" size="25" maxlength="[{$edit->trwcookieconsent__oxtitle->fldmax_length}]" name="editval[trwcookieconsent__oxtitle]" value="[{$edit->trwcookieconsent__oxtitle->value}]" [{$readonly}] />
                            [{oxinputhelp ident="HELP_GENERAL_NAME"}]
                        </td>
                    </tr>
                    <tr>
                        <td class="edittext">
                            [{oxmultilang ident="GENERAL_COOKIECONSENT_TYPE"}]
                        </td>
                        <td class="edittext">
                            <select name="editval[trwcookieconsent__oxtype]">
                                [{foreach from=$cookieTypes item="sCookieType" name="cookietypes"}]
                                    <option value="[{$sCookieType}]" [{if $edit->trwcookieconsent__oxtype->value == $sCookieType}]selected[{/if}]>
                                        [{oxmultilang ident="TRWCOOKIECONSENT_TYPE_"|cat:$sCookieType|upper}]
                                    </option>
                                [{/foreach}]
                            </select>
                            [{oxinputhelp ident="HELP_GENERAL_COOKIECONSENT_TYPE"}]
                        </td>
                    </tr>

                    <tr>
                        <td class="edittext">
                            [{oxmultilang ident="GENERAL_SORT"}]
                        </td>
                        <td class="edittext">
                            <input type="text" class="editinput" size="10" maxlength="[{$edit->trwcookieconsent__oxsort->fldmax_length}]" name="editval[trwcookieconsent__oxsort]" value="[{$edit->trwcookieconsent__oxsort->value}]" [{$readonly}]>
                            [{oxinputhelp ident="HELP_GENERAL_SORT"}]
                        </td>
                    </tr>

                    [{/block}]
                    <tr>
                        <td class="edittext">&nbsp;</td>
                        <td class="edittext"><br>
                            <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_SAVE"}]" onClick="Javascript:document.myedit.fnc.value='save'" [{$readonly}]><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="edittext">&nbsp;</td>
                        <td class="edittext">
                            <br />
                            [{include file="language_edit.tpl"}]
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>

[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]
